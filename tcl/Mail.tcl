set BaseDir [file normalize [file dir [info script]]]
set Bits [expr {$tcl_platform(wordSize)*8}]
if {$tcl_platform(platform) eq "windows"} {
  set BinDir "Win${Bits}"
} elseif {$tcl_platform(platform) eq "unix" && $tcl_platform(os) eq "Linux"} {
  set BinDir "Linux${Bits}"
} else {
  error "Unknown platform $tcl_platform(platform) ($tcl_platform(os))"
}

lappend auto_path $BaseDir [file join $BaseDir lib $BinDir]

package require smtp
package require mime

proc send_email {server user password to body args} {
 
  puts $args 

  array set options {
    -usetls 0
    -port 25
    -subject "(no subject)"
    -debug 0
  }
  array set options $args
  set bodyToken [mime::initialize -canonical text/plain -string $body]
  set parts [list]
  foreach a [array names options -attachment*] {
    set name [file tail $options($a)]
    lappend parts [mime::initialize \
                       -canonical "application/octet-stream; name=\"$name\"" \
                       -file $options($a)]
  }
  if {[llength $parts]} {
    set mailToken [mime::initialize -canonical multipart/mixed \
                   -parts [linsert $parts 0 $bodyToken]]
  } else {
    set mailToken $bodyToken
  }
  mime::setheader $mailToken Subject $options(-subject)
  set smtp_cmd [list \
                    smtp::sendmessage $mailToken \
                    -ports $options(-port) \
                    -usetls $options(-usetls) \
                    -debug $options(-debug) \
                    -username $user \
                    -password $password \
                    -header [list To $to] \
                    -servers $server]
  foreach cc [array names options -cc*] {
    puts "processing cc = $options($cc)"
    lappend smtp_cmd -header [list cc $options($cc)]
  }
  eval $smtp_cmd
  mime::finalize $mailToken
}
